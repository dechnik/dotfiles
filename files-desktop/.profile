export PATH="$HOME/.local/bin:$PATH"

export EDITOR=/usr/bin/nvim
export BROWSER=vivaldi-stable
export TERMINAL=alacritty
export MAIL=thunderbird
export QT_QPA_PLATFORMTHEME="qt5ct"
export GTK2_RC_FILES="$HOME/.gtkrc-2.0"

export FZF_DEFAULT_OPTS="--layout=reverse --height 40%"
export AWS_CONFIG_FILE="$HOME/.config/aws/config"
export AWS_SHARED_CREDENTIALS_FILE="$HOME/.config/aws/credentials"
export GOPATH="$HOME/.local/share/go"
export NVM_DIR="$HOME/.local/share/nvm"
export CARGO_HOME="$HOME/.local/share/cargo"
export CUDA_CACHE_PATH="$HOME/.cache/nv"
#export VK_ICD_FILENAMES="/usr/share/vulkan/icd.d/nvidia_icd.json"
export PASSWORD_STORE_DIR="$HOME/.local/share/password-store"
export NOTMUCH_CONFIG="$HOME/.config/notmuch-config"

[ -d "$HOME/.local/share/platform-tools" ] && export PATH="$HOME/.local/share/platform-tools:$PATH"

[ -f "$HOME/.pprofile" ] && source "$HOME/.pprofile"
