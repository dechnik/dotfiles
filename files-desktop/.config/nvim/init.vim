"        _
" __   _(_)_ __ ___  _ __ ___
" \ \ / / | '_ ` _ \| '__/ __|
"  \ V /| | | | | | | | | (__
"   \_/ |_|_| |_| |_|_|  \___|

let mapleader =" "

if ! filereadable(expand('~/.config/nvim/autoload/plug.vim'))
	echo "Downloading junegunn/vim-plug to manage plugins..."
	silent !mkdir -p ~/.config/nvim/autoload/
	silent !curl "https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim" > ~/.config/nvim/autoload/plug.vim
	autocmd VimEnter * PlugInstall
endif

call plug#begin('~/.config/nvim/plugged')
Plug 'tpope/vim-surround'
Plug 'tpope/vim-vinegar'
Plug 'ap/vim-css-color'
Plug 'wellle/targets.vim'
Plug 'vim-airline/vim-airline'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'morhetz/gruvbox'
Plug 'ryanoasis/vim-devicons'
Plug 'junegunn/fzf.vim'
Plug 'junegunn/fzf'
Plug 'airblade/vim-gitgutter'
Plug 'tpope/vim-fugitive'
Plug 'unblevable/quick-scope'
Plug 'majutsushi/tagbar'
Plug 'vimwiki/vimwiki'
Plug 'troydm/easybuffer.vim'
Plug 'hashivim/vim-terraform'
call plug#end()

" Some basics:
    set title
	set nocompatible
    set cursorline
	filetype plugin on
	syntax on
	set mouse=a
	set encoding=utf-8
	set number
	set relativenumber
    "set background=dark

" Gruvbox
    if exists('+termguicolors')
        let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
        let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
        set termguicolors
    endif
    "let g:gruvbox_invert_selection='0'
	"colorscheme gruvbox
    autocmd vimenter * ++nested colorscheme gruvbox

    autocmd vimenter * hi Normal guibg=NONE ctermbg=NONE
    autocmd vimenter * hi EndOfBuffer guibg=NONE ctermbg=NONE
    autocmd vimenter * hi NonText guibg=NONE ctermbg=NONE

" Airline
    let g:airline#extensions#wordcount#enabled = 1
    let g:airline_powerline_fonts = 1

" Wimwiki
    let g:vimwiki_list = [{'path': '~/.local/share/vimwiki/'}]

" Always show sign for gitgutter
    set signcolumn=yes

" Persistent undo
	set undodir=~/.config/nvim/undodir
	set undofile

" Terraform
    let g:terraform_fmt_on_save=1

" system clipboard
	set clipboard+=unnamedplus

" Enable autocompletion:
	set wildmode=longest,list,full

" Replace all is aliased to S.
	"nnoremap S :%s//g<Left><Left>

" Disables automatic commenting on newline:
	autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

" Specific options for file tpyes
	autocmd FileType yaml set tabstop=2 shiftwidth=2

" Splits open at the bottom and right, which is non-retarded, unlike vim defaults.
	set splitbelow
	set splitright

" Having longer updatetime (default is 4000 ms = 4 s) leads to noticeable
" delays and poor user experience.
    set updatetime=50

" Indentation settings
	set autoindent
	set tabstop=4
	set shiftwidth=4
	set expandtab

" Show matching bracket
	set showmatch

	set ignorecase " searches are case insensitive...
	set smartcase  " ... unless they contain at least one capital letter

" Shortcutting split navigation, saving a keypress:
	map <C-h> <C-w>h
	map <C-j> <C-w>j
	map <C-k> <C-w>k
	map <C-l> <C-w>l

" fzf mapping
    let g:fzf_layout = { 'window': { 'width': 0.8, 'height': 0.8 } }
    let $FZF_DEFAULT_OPTS='--reverse'
	nnoremap ; :Buffers<CR>
	nnoremap <leader>f :Files<CR>
	nnoremap <leader>T :Tags<CR>
	nnoremap <leader>t :BTags<CR>
	nnoremap s :Ag<CR>
" git
    nmap <leader>gj :diffget //3<CR>
    nmap <leader>gf :diffget //2<CR>
    nmap <leader>gs :G<CR>

" Use urlscan to choose and open a url:
	:noremap <leader>u :w<Home> !urlscan -r 'linkhandler {}'<CR>
	:noremap ,, !urlscan -r 'linkhandler {}'<CR>

" C-c and C-v - Copy/Paste to global clipboard
	vmap <C-c> "+yi
	imap <C-v> <esc>"+gpi

" Check file in shellcheck:
	map <leader>s :!clear && shellcheck %<CR>

" Coc
    nmap <leader>gd <Plug>(coc-definition)
    nmap <leader>gr <Plug>(coc-references)

" Quick-scope
    let g:qs_highlight_on_keys = ['f', 'F', 't', 'T']

" Automatically deletes all trailing whitespace on save.
	autocmd BufWritePre * %s/\s\+$//e

" Run xrdb whenever Xdefaults or Xresources are updated.
    autocmd BufRead,BufNewFile xresources,xdefaults set filetype=xdefaults
	autocmd BufWritePost Xresources,Xdefaults,xresources,xdefaults !xrdb %

" Recompile dwmblocks on config edit.
	autocmd BufWritePost ~/projects/dwmblocks/config.h !cd ~/projects/dwmblocks/; sudo make install && { killall -q dwmblocks;setsid -f dwmblocks }

" Update binds when sxhkdrc is updated.
	autocmd BufWritePost *sxhkdrc !pkill -USR1 sxhkd

" NERDtree
"	autocmd StdinReadPre * let s:std_in=1
"	autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'NERDTree' argv()[0] | wincmd p | ene | endif
"	map <C-n> :NERDTreeToggle<CR>

" F11 - Show tagbar list
    let g:tagbar_left=1
    nmap <F11> :TagbarToggle<CR>
    imap <F11> <esc>:TagbarToggle<CR>
    vmap <F11> <esc>:TagbarToggle<CR>

" F5 - easy buffer
    nmap <F5> <Esc>:EasyBuffer<cr>

" Disable Arrow keys in Escape mode
    "map <up> <nop>
    "map <down> <nop>
    "map <left> <nop>
    "map <right> <nop>

" Disable Arrow keys in Insert mode
    "imap <up> <nop>
    "imap <down> <nop>
    "imap <left> <nop>
    "imap <right> <nop>
