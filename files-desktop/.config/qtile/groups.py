from libqtile.config import Group, EzKey as Key
from libqtile.lazy import lazy

from keys import keys

groups = []

groups.extend([
    Group(name="1", label="1", layout="monadtall", spawn="", persist=True),
    Group(name="2", label="2", layout="monadtall", spawn="", persist=True),
    Group(name="3", label="3", layout="monadtall", spawn="", persist=True),
    Group(name="4", label="4", layout="monadtall", spawn="", persist=True),
    Group(name="5", label="5", layout="monadtall", spawn="", persist=True),
    Group(name="6", label="G", layout="monadtall", spawn="", persist=True),
    Group(name="7", label="7", layout="monadtall", spawn="", persist=True),
    Group(name="8", label="8", layout="monadtall", spawn="", persist=True),
    Group(name="9", label="9", layout="monadtall", spawn="", persist=True),
    Group(name="0", label="0", layout="monadtall", spawn="", persist=True),
    ])

# FOR QWERTY KEYBOARDS
#group_names = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0",]

# FOR AZERTY KEYBOARDS
#group_names = ["ampersand", "eacute", "quotedbl", "apostrophe", "parenleft", "section", "egrave", "exclam", "ccedilla", "agrave",]

#group_labels = ["1 ", "2 ", "3 ", "4 ", "5 ", "G ", "7 ", "8 ", "9 ", "0 ",]
#group_labels = ["", "", "", "", "", "", "", "", "", "",]
#🎮🌎
#group_labels = ["Web", "Edit/chat", "Image", "Gimp", "Meld", "Video", "Vb", "Files", "Mail", "Music",]

#group_layouts = ["monadtall", "monadtall", "monadtall", "monadtall", "monadtall", "floating", "monadtall", "monadtall", "monadtall", "monadtall",]
#group_layouts = ["monadtall", "matrix", "monadtall", "bsp", "monadtall", "matrix", "monadtall", "bsp", "monadtall", "monadtall",]

#for i in range(len(group_names)):
#    groups.append(
#        Group(
#            name=group_names[i],
#            layout=group_layouts[i].lower(),
#            label=group_labels[i],
#        ))

for i in groups:
    keys.extend([

#CHANGE WORKSPACES
        Key(f"M-{i.name}", lazy.group[i.name].toscreen()),
        Key("M-<Tab>", lazy.screen.next_group()),
        Key("A-<Tab>", lazy.screen.next_group()),
        Key("A-S-<Tab>", lazy.screen.prev_group()),

# MOVE WINDOW TO SELECTED WORKSPACE 1-10 AND STAY ON WORKSPACE
        Key(f"M-S-{i.name}", lazy.window.togroup(i.name)),
# MOVE WINDOW TO SELECTED WORKSPACE 1-10 AND FOLLOW MOVED WINDOW TO WORKSPACE
        #Key([mod, "shift"], i.name, lazy.window.togroup(i.name) , lazy.group[i.name].toscreen()),
    ])

