import os
from libqtile.widget import base
from libqtile.widget.generic_poll_text import GenPollUrl

QUERY_URL = 'http://api.openweathermap.org/data/2.5/weather?id={}&lang={}&appid={}&units={}'
API_KEY = os.environ['WEATHER_API']

def get_wicons():
    wicons = {
            "200": {
                "label": "thunderstorm with light rain",
                "icon": "⚡"
                },

            "201": {
                "label": "thunderstorm with rain",
                "icon": "⚡"
                },

            "202": {
                "label": "thunderstorm with heavy rain",
                "icon": "⚡"
                },

            "210": {
                "label": "light thunderstorm",
                "icon": "⚡"
                },

            "211": {
                "label": "thunderstorm",
                "icon": "⚡"
                },

            "212": {
                "label": "heavy thunderstorm",
                "icon": "⚡"
                },

            "221": {
                "label": "ragged thunderstorm",
                "icon": "⚡"
                },

            "230": {
                "label": "thunderstorm with light drizzle",
                "icon": "⚡"
                },

            "231": {
                "label": "thunderstorm with drizzle",
                "icon": "⚡"
                },

            "232": {
                "label": "thunderstorm with heavy drizzle",
                "icon": "⚡"
                },

            "300": {
                    "label": "light intensity drizzle",
                    "icon": "🌧️"
                    },

            "301": {
                    "label": "drizzle",
                    "icon": "🌧️"
                    },

            "302": {
                    "label": "heavy intensity drizzle",
                    "icon": "🌧️"
                    },

            "310": {
                    "label": "light intensity drizzle rain",
                    "icon": "🌧️"
                    },

            "311": {
                    "label": "drizzle rain",
                    "icon": "🌧️"
                    },

            "312": {
                    "label": "heavy intensity drizzle rain",
                    "icon": "🌧️"
                    },

            "313": {
                    "label": "shower rain and drizzle",
                    "icon": "🌧️"
                    },

            "314": {
                    "label": "heavy shower rain and drizzle",
                    "icon": "🌧️"
                    },

            "321": {
                    "label": "shower drizzle",
                    "icon": "🌧️"
                    },

            "500": {
                    "label": "light rain",
                    "icon": "🌧️"
                    },

            "501": {
                    "label": "moderate rain",
                    "icon": "🌧️"
                    },

            "502": {
                    "label": "heavy intensity rain",
                    "icon": "🌧️"
                    },

            "503": {
                    "label": "very heavy rain",
                    "icon": "🌧️"
                    },

            "504": {
                    "label": "extreme rain",
                    "icon": "🌧️"
                    },

            "511": {
                    "label": "freezing rain",
                    "icon": "🌧️"
                    },

            "520": {
                    "label": "light intensity shower rain",
                    "icon": "🌧️"
                    },

            "521": {
                    "label": "shower rain",
                    "icon": "🌧️"
                    },

            "522": {
                    "label": "heavy intensity shower rain",
                    "icon": "🌧️"
                    },

            "531": {
                    "label": "ragged shower rain",
                    "icon": "🌧️"
                    },

            "600": {
                    "label": "light snow",
                    "icon": "❄️"
                    },

            "601": {
                    "label": "snow",
                    "icon": "❄️"
                    },

            "602": {
                    "label": "heavy snow",
                    "icon": "❄️"
                    },

            "611": {
                    "label": "sleet",
                    "icon": "sleet"
                    },

            "612": {
                    "label": "shower sleet",
                    "icon": "sleet"
                    },

            "615": {
                    "label": "light rain and snow",
                    "icon": "rain-mix"
                    },

            "616": {
                    "label": "rain and snow",
                    "icon": "rain-mix"
                    },

            "620": {
                    "label": "light shower snow",
                    "icon": "rain-mix"
                    },

            "621": {
                    "label": "shower snow",
                    "icon": "rain-mix"
                    },

            "622": {
                    "label": "heavy shower snow",
                    "icon": "rain-mix"
                    },

            "701": {
                    "label": "mist",
                    "icon": ""
                    },

            "711": {
                    "label": "smoke",
                    "icon": "smoke"
                    },

            "721": {
                    "label": "haze",
                    "icon": "day-haze"
                    },

            "731": {
                    "label": "sand, dust whirls",
                    "icon": "cloudy-gusts"
                    },

            "741": {
                    "label": "fog",
                    "icon": "fog"
                    },

            "751": {
                    "label": "sand",
                    "icon": "cloudy-gusts"
                    },

            "761": {
                    "label": "dust",
                    "icon": "dust"
                    },

            "762": {
                    "label": "volcanic ash",
                    "icon": "smog"
                    },

            "771": {
                    "label": "squalls",
                    "icon": "day-windy"
                    },

            "781": {
                    "label": "tornado",
                    "icon": "tornado"
                    },

            "800": {
                    "label": "clear sky",
                    "icon": "☀️"
                    },

            "801": {
                    "label": "few clouds",
                    "icon": "☁️"
                    },

            "802": {
                    "label": "scattered clouds",
                    "icon": "☁️"
                    },

            "803": {
                    "label": "broken clouds",
                    "icon": "☁️"
                    },

            "804": {
                    "label": "overcast clouds",
                    "icon": "☁️"
                    },


            "900": {
                    "label": "tornado",
                    "icon": "tornado"
                    },

            "901": {
                    "label": "tropical storm",
                    "icon": "hurricane"
                    },

            "902": {
                    "label": "hurricane",
                    "icon": "hurricane"
                    },

            "903": {
                    "label": "cold",
                    "icon": "snowflake-cold"
                    },

            "904": {
                    "label": "hot",
                    "icon": "hot"
                    },

            "905": {
                    "label": "windy",
                    "icon": "windy"
                    },

            "906": {
                    "label": "hail",
                    "icon": "hail"
                    },

            "951": {
                    "label": "calm",
                    "icon": "☀️"
                    },

            "952": {
                    "label": "light breeze",
                    "icon": "cloudy-gusts"
                    },

            "953": {
                    "label": "gentle breeze",
                    "icon": "cloudy-gusts"
                    },

            "954": {
                    "label": "moderate breeze",
                    "icon": "cloudy-gusts"
                    },

            "955": {
                    "label": "fresh breeze",
                    "icon": "cloudy-gusts"
                    },

            "956": {
                    "label": "strong breeze",
                    "icon": "cloudy-gusts"
                    },

            "957": {
                    "label": "high wind, near gale",
                    "icon": "cloudy-gusts"
                    },

            "958": {
                    "label": "gale",
                    "icon": "cloudy-gusts"
                    },

            "959": {
                    "label": "severe gale",
                    "icon": "cloudy-gusts"
                    },

            "960": {
                    "label": "storm",
                    "icon": "thunderstorm"
                    },

            "961": {
                    "label": "violent storm",
                    "icon": "thunderstorm"
                    },

            "962": {
                    "label": "hurricane",
                    "icon": "cloudy-gusts"
                    }
            }
    return wicons

class OpenWeather(GenPollUrl):
    orientations = base.ORIENTATION_HORIZONTAL
    defaults = [
        # One of (woeid, location, coordinates) must be set.
        (
            'city',
            '3081368',
            'City Id'
        ),
        (
            'format',
            '{icon} {description}, {temp}°{unit_key}',
            'Display format'
        ),
        ('units', 'Metric', 'standard, metric, imperial'),
        ('unit_key', 'C', 'unit_key'),
        ('lang', 'en', 'language'),
    ]
    icons = get_wicons()

    def __init__(self, **config):
        GenPollUrl.__init__(self, **config)
        self.add_defaults(OpenWeather.defaults)

    @property
    def url(self):
        return QUERY_URL.format(self.city, self.lang, API_KEY, self.units)

    def parse(self, body):
        icon_id = str(body["weather"][0]["id"])
        data = {
                'icon': self.icons[icon_id]["icon"] if icon_id in self.icons.keys() else icon_id,
                'description': body["weather"][0]["description"].capitalize(),
                'temp': int(float(body["main"]["temp"])),
                'unit_key': self.unit_key
        }
        return self.format.format(**data)
