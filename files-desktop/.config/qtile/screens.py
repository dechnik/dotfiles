from Xlib import display as xdisplay
from libqtile.config import Screen, Rule
from bars import primary_top, secondary_top

def get_num_monitors():
    num_monitors = 0
    try:
        display = xdisplay.Display()
        screen = display.screen()
        resources = screen.root.xrandr_get_screen_resources()

        for output in resources.outputs:
            monitor = display.xrandr_get_output_info(output, resources.config_timestamp)
            preferred = False
            if hasattr(monitor, "preferred"):
                preferred = monitor.preferred
            elif hasattr(monitor, "num_preferred"):
                preferred = monitor.num_preferred
            if preferred:
                num_monitors += 1
    except Exception as e:
        # always setup at least one monitor
        return 1
    else:
        return num_monitors

def init_screens():
    num_monitors = get_num_monitors()
    screens_list = [
            Screen(top=primary_top)
            ]
    if num_monitors > 1:
        for m in range(num_monitors - 1):
            screens_list.append(
                Screen(top=secondary_top)
            )
    return screens_list

screens = init_screens()
