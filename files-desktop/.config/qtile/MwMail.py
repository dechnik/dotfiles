from libqtile.widget import base
import os

class MwMail(base.ThreadPoolText):
    orientations = base.ORIENTATION_HORIZONTAL
    defaults = [
            ('mail_dir', '~/.local/share/mail', 'path to the Maildir folder'),
            ('accounts_dir', '~/.config/mutt/accounts', 'path to the accounts folder'),
            ('inbox', 'INBOX', 'inbox folder'),
            ('exclude', ['.notmuch'], 'exclude folders'),
            ('format', '{value}', 'format string'),
    ]
    accounts = {}

    def __init__(self, **config):
        base.ThreadPoolText.__init__(self, "", **config)
        self.add_defaults(MwMail.defaults)
        accounts_dir = os.path.expanduser(self.accounts_dir)
        for f in os.listdir(accounts_dir):
            tmp = f.split('-')
            nr = tmp[0]
            name = tmp[1].replace('.muttrc','')
            self.accounts[name] = nr

    def poll(self):
        mail_dir = os.path.expanduser(self.mail_dir)
        data = ""
        for dr in os.listdir(mail_dir):
            mail_count = 0
            if dr not in self.exclude:
                path = os.path.join(mail_dir,dr,self.inbox,'new')
                for file in os.listdir(path):
                    mail_count += 1
                if mail_count > 0:
                    data += " {}:{}".format(self.accounts[dr], mail_count)
        return self.format.format(value=data)
