import os
import socket
import subprocess
from Weather import OpenWeather
from MwMail import MwMail
import albattery
from colors import colors
from libqtile import bar, widget
from libqtile.widget import Spacer

def get_news():
    output = subprocess.check_output(["newsboat", "-x", "print-unread"]).decode('utf-8').strip()
    return output.split(" ")[0]

def init_widgets_defaults():
    return dict(font="Noto Sans Mono",
                fontsize = 9,
                padding = 2,
                foreground = colors["light1"],
                background=colors["dark0"])

widget_defaults = init_widgets_defaults()

def init_primary_widgets_list():
    prompt = "{0}@{1}: ".format(os.environ["USER"], socket.gethostname())
    widgets_list = [

            widget.Sep(
                linewidth = 1,
                padding = 10,
                foreground = colors["light4"],
                background = colors["dark0"]
                ),
            widget.Image(
                filename = "~/.config/qtile/icons/garuda-red.png",
                iconsize = 9,
                background = colors["dark0"],
                mouse_callbacks = {'Button1': lambda qtile: qtile.cmd_spawn('jgmenu_run')}
                ),
            widget.Sep(
                linewidth = 1,
                padding = 10,
                foreground = colors["light4"],
                background = colors["dark0"]
                ),
            widget.GroupBox(font="Noto Sans Mono Bold",
                fontsize = 14,
                #margin_y = 3,
                #margin_x = 0,
                #padding_y = 12,
                #padding_x = 2,
                padding = 6,
                borderwidth = 2,
                disable_drag = True,
                active = colors["neutral_yellow"],
                inactive = colors["dark1"],
                rounded = False,
                highlight_method = "line",
                urgent_alert_method = "line",
                highlight_color = [colors["dark0"], colors["dark0"]],
                this_current_screen_border = colors["bright_red"],
                this_screen_border = ["neutral_yellow"],
                other_current_screen_border = colors["light4"],
                other_screen_border = colors["light4"],
                urgent_border = colors["neutral_red"],
                urgent_text = colors["neutral_red"],
                foreground = colors["light1"],
                background = colors["dark0"]
                ),
            widget.Sep(
                linewidth = 1,
                padding = 10,
                foreground = colors["light4"],
                background = colors["dark0"]
                ),
            widget.WindowName(font="Fantasque Sans Mono",
                fontsize = 16,
                padding = 10,
                foreground = colors["neutral_red"],
                background = colors["dark0"],
                ),

            widget.Sep(
                linewidth = 1,
                padding = 10,
                ),
            widget.CurrentLayoutIcon(
                    custom_icon_paths = [os.path.expanduser("~/.config/qtile/icons")],
                    padding = 0,
                    scale = 0.7
                    ),
            widget.CurrentLayout(
                    font = "Noto Sans Mono Bold",
                    fontsize = 12,
                    ),
            widget.Sep(
                linewidth = 1,
                padding = 10,
                ),
            OpenWeather(
                    update_interval=3600,
                    font = "Noto Sans Mono",
                    fontsize = 12,
                    padding = 2,
                    ),
            widget.Sep(
                linewidth = 1,
                padding = 10,
                ),
            widget.TextBox(
                    font="FantasqueSansMono",
                    text=" ",
                    padding = 0,
                    fontsize=12
                    ),
            widget.GenPollText(
                    update_interval=3600,
                    font = "Noto Sans Mono",
                    fontsize = 12,
                    padding = 2,
                    mouse_callbacks = {'Button1': lambda qtile: qtile.cmd_spawn(myTerm + ' -e newsboat')},
                    func = get_news
                    ),
            widget.Sep(
                linewidth = 1,
                padding = 10,
                ),
            widget.TextBox(
                    font="FontAwesome",
                    text="",
                    padding = 0,
                    fontsize=10
                    ),
            MwMail(
                    update_interval=600,
                    font = "Noto Sans Mono",
                    fontsize = 12,
                    padding = 2,
                    mouse_callbacks = {'Button1': lambda qtile: qtile.cmd_spawn(myTerm + ' -e neomutt')}
                    ),
            widget.Sep(
                linewidth = 1,
                padding = 10,
                ),
            widget.CPU(
                    font="Noto Sans Mono",
                    #format = '{MemUsed}M/{MemTotal}M',
                    format = ' {freq_current}GHz {load_percent}%',
                    update_interval = 1,
                    fontsize = 12,
                    mouse_callbacks = {'Button1': lambda qtile: qtile.cmd_spawn(myTerm + ' -e htop')},
                    ),
            widget.ThermalSensor(
                    font="Noto Sans Mono",
                    update_interval = 1,
                    foreground = colors["light4"],
                    fontsize = 12,
                    tag_sensor = 'Tctl'
                    ),
            widget.Sep(
                linewidth = 1,
                padding = 10,
                ),
            widget.TextBox(
                    font="FontAwesome",
                    text="  ",
                    padding = 0,
                    fontsize=16
                    ),
            widget.Memory(
                    font="Noto Sans Mono",
                    format = '{MemUsed}M/{MemTotal}M',
                    update_interval = 1,
                    fontsize = 12,
                    mouse_callbacks = {'Button1': lambda qtile: qtile.cmd_spawn(myTerm + ' -e htop')},
                    ),
            widget.Sep(
                linewidth = 1,
                padding = 10,
                ),
            widget.TextBox(
                    font="FontAwesome",
                    text="  ",
                    padding = 0,
                    fontsize=16
                    ),
            widget.Clock(
                    fontsize = 12,
                    format="%Y-%m-%d %H:%M"
                    ),
            widget.Sep(
                linewidth = 1,
                padding = 10,
                ),
            widget.Systray(
                    icon_size=20,
                    padding = 6
                    ),
            ]
    return widgets_list

def init_secondary_widgets_list():
    prompt = "{0}@{1}: ".format(os.environ["USER"], socket.gethostname())
    widgets_list = [

            widget.Sep(
                linewidth = 1,
                padding = 10,
                ),
            widget.Image(
                filename = "~/.config/qtile/icons/garuda-red.png",
                iconsize = 9,
                background = colors["dark0"],
                mouse_callbacks = {'Button1': lambda qtile: qtile.cmd_spawn('jgmenu_run')}
                ),
            widget.Sep(
                linewidth = 1,
                padding = 10,
                ),
            widget.GroupBox(font="Noto Sans Mono Bold",
                fontsize = 14,
                #margin_y = 3,
                #margin_x = 0,
                #padding_y = 12,
                #padding_x = 2,
                padding = 6,
                borderwidth = 2,
                disable_drag = True,
                active = colors["neutral_yellow"],
                inactive = colors["dark1"],
                rounded = False,
                highlight_method = "line",
                urgent_alert_method = "line",
                highlight_color = [colors["dark0"], colors["dark0"]],
                this_current_screen_border = colors["bright_red"],
                this_screen_border = ["neutral_yellow"],
                other_current_screen_border = colors["light4"],
                other_screen_border = colors["light4"],
                urgent_border = colors["neutral_red"],
                urgent_text = colors["neutral_red"],
                foreground = colors["light1"],
                background = colors["dark0"]
                ),
            widget.Sep(
                linewidth = 1,
                padding = 10,
                ),
            widget.WindowName(font="Fantasque Sans Mono",
                fontsize = 16,
                padding = 10,
                foreground = colors["neutral_red"],
                background = colors["dark0"],
                ),
            widget.Sep(
                linewidth = 1,
                padding = 10,
                ),
            widget.CurrentLayoutIcon(
                    custom_icon_paths = [os.path.expanduser("~/.config/qtile/icons")],
                    padding = 0,
                    scale = 0.7
                    ),
            widget.CurrentLayout(
                    font = "Noto Sans Bold",
                    fontsize = 12,
                    ),
            widget.Sep(
                linewidth = 1,
                padding = 10,
                ),
            widget.TextBox(
                    font="FontAwesome",
                    text="  ",
                    padding = 0,
                    fontsize=16
                    ),
            widget.Clock(
                    fontsize = 12,
                    format="%Y-%m-%d %H:%M"
                    ),
            ]
    return widgets_list

primary_top=bar.Bar(widgets=init_primary_widgets_list(), size=20, opacity=1, background=colors["dark0"])
secondary_top=bar.Bar(widgets=init_secondary_widgets_list(), size=20, opacity=1, background=colors["dark0"])
