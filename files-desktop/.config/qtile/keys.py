from libqtile.config import EzKey as Key, EzDrag as Drag, EzClick as Click
from libqtile.lazy import lazy
import os

myTerm = "alacritty" # My terminal of choice
home = os.path.expanduser('~')

@lazy.function
def window_to_prev_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i - 1].name)

@lazy.function
def window_to_next_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i + 1].name)

@lazy.function
def move_toscreen_right(qtile):
    kick_to_next_screen(qtile)

@lazy.function
def move_toscreen_left(qtile):
    kick_to_next_screen(qtile,-1)

#custom functions
# kick a window to another screen (handy during presentations)
def kick_to_next_screen(qtile, direction=1):
    other_scr_index = (qtile.screens.index(qtile.currentScreen) + direction) % len(qtile.screens)
    othergroup = None
    for group in qtile.cmd_groups().values():
        if group['screen'] == other_scr_index:
            othergroup = group['name']
            break
    if othergroup:
        qtile.moveToGroup(othergroup)

keys = [

    # SUPER + FUNCTION KEYS
    Key("M-f", lazy.window.toggle_fullscreen()),
    Key("M-m", lazy.spawn('pragha')),
    Key("M-q", lazy.window.kill()),
    Key("M-t", lazy.spawn('termite')),
    Key("M-e", lazy.spawn(myTerm + ' -e neomutt')),
    Key("M-v", lazy.spawn('pavucontrol')),
    Key("M-d", lazy.spawn('rofi -show run')),
    Key("M-<Escape>", lazy.spawn('xkill')),
    Key("M-<Return>", lazy.spawn(myTerm)),
    Key("M-<KP_Enter>", lazy.spawn(myTerm)),
    #Key("M-w", lazy.spawn(home + '/.config/qtile/scripts/pywal-colors.py')),
    Key("M-x", lazy.shutdown()),
    Key("M-<F2>", lazy.spawn('dmenussh')),

    # SUPER + SHIFT KEYS

    Key("M-S-<Return>", lazy.spawn('pcmanfm')),
    #Key([mod, "shift"], "d", lazy.spawn("dmenu_run -i -nb '#191919' -nf '#fea63c' -sb '#fea63c' -sf '#191919' -fn 'NotoMonoRegular:bold:pixelsize=14'")),
    #Key([mod, "shift"], "d", lazy.spawn(home + '/.config/qtile/scripts/dmenu.sh')),
    Key("M-S-d", lazy.spawn('nwggrid -p -o 0.4')),
    Key("M-S-e", lazy.spawn(myTerm + ' -e newsboat')),
    Key("M-S-q", lazy.window.kill()),
    Key("M-S-r", lazy.restart()),
    Key("M-S-r", lazy.restart()),
    Key("M-S-x", lazy.shutdown()),

    # CONTROL + ALT KEYS

    Key("A-C-c", lazy.spawn('catfish')),
    Key("A-C-i", lazy.spawn('nitrogen')),
    Key("A-C-o", lazy.spawn(home + '/.config/qtile/scripts/picom-toggle.sh')),
    Key("A-C-t", lazy.spawn('termite')),
    Key("A-C-u", lazy.spawn('pavucontrol')),
    Key("A-C-<Return>", lazy.spawn('termite')),

    # ALT + ... KEYS

    Key("A-p", lazy.spawn('pamac-manager')),
    Key("A-f", lazy.spawn('firefox')),
    Key("A-e", lazy.spawn('emacs')),
    Key("A-m", lazy.spawn('pcmanfm')),
    Key("A-w", lazy.spawn('garuda-welcome')),


# CONTROL + SHIFT KEYS

    Key("C-S-<Escape>", lazy.spawn('lxtask')),

# SCREENSHOTS

    Key("<Print>", lazy.spawn('flameshot full -p ' + home + '/Pictures')),
    Key("C-<Print>", lazy.spawn('flameshot full -p ' + home + '/Pictures')),
#    Key([mod2, "shift"], "Print", lazy.spawn('gnome-screenshot -i')),

# MULTIMEDIA KEYS

# INCREASE/DECREASE BRIGHTNESS
    Key("<XF86MonBrightnessUp>", lazy.spawn("xbacklight -inc 5")),
    Key("<XF86MonBrightnessDown>", lazy.spawn("xbacklight -dec 5")),

# INCREASE/DECREASE/MUTE VOLUME
    Key("<XF86AudioMute>", lazy.spawn("amixer -q set Master toggle")),
    Key("<XF86AudioLowerVolume>", lazy.spawn("amixer -q set Master 5%-")),
    Key("<XF86AudioRaiseVolume>", lazy.spawn("amixer -q set Master 5%+")),

    Key("<XF86AudioPlay>", lazy.spawn("playerctl play-pause")),
    Key("<XF86AudioNext>", lazy.spawn("playerctl next")),
    Key("<XF86AudioPrev>", lazy.spawn("playerctl previous")),
    Key("<XF86AudioStop>", lazy.spawn("playerctl stop")),

#    Key([], "XF86AudioPlay", lazy.spawn("mpc toggle")),
#    Key([], "XF86AudioNext", lazy.spawn("mpc next")),
#    Key([], "XF86AudioPrev", lazy.spawn("mpc prev")),
#    Key([], "XF86AudioStop", lazy.spawn("mpc stop")),

# QTILE LAYOUT KEYS
    Key("M-n", lazy.layout.normalize()),
    Key("M-<space>", lazy.next_layout()),

# CHANGE FOCUS
    Key("M-<Up>", lazy.layout.up()),
    Key("M-<Down>", lazy.layout.down()),
    Key("M-<Left>", lazy.layout.left()),
    Key("M-<Right>", lazy.layout.right()),
    Key("M-k", lazy.layout.up()),
    Key("M-j", lazy.layout.down()),
    Key("M-h", lazy.layout.left()),
    Key("M-l", lazy.layout.right()),


# RESIZE UP, DOWN, LEFT, RIGHT
    Key("M-C-l",
            lazy.layout.grow_right(),
            lazy.layout.grow(),
            lazy.layout.increase_ratio(),
            lazy.layout.delete(),
            ),
    Key("M-C-<Right>",
            lazy.layout.grow_right(),
            lazy.layout.grow(),
            lazy.layout.increase_ratio(),
            lazy.layout.delete(),
            ),
    Key("M-C-h",
            lazy.layout.grow_left(),
            lazy.layout.shrink(),
            lazy.layout.decrease_ratio(),
            lazy.layout.add(),
            ),
    Key("M-C-<Left>",
            lazy.layout.grow_left(),
            lazy.layout.shrink(),
            lazy.layout.decrease_ratio(),
            lazy.layout.add(),
            ),
    Key("M-C-k",
            lazy.layout.grow_up(),
            lazy.layout.grow(),
            lazy.layout.decrease_nmaster(),
            ),
    Key("M-C-<Up>",
            lazy.layout.grow_up(),
            lazy.layout.grow(),
            lazy.layout.decrease_nmaster(),
            ),
    Key("M-C-j",
            lazy.layout.grow_down(),
            lazy.layout.shrink(),
            lazy.layout.increase_nmaster(),
            ),
    Key("M-C-<Down>",
            lazy.layout.grow_down(),
            lazy.layout.shrink(),
            lazy.layout.increase_nmaster(),
            ),


    # FLIP LAYOUT FOR MONADTALL/MONADWIDE
    Key("M-S-f", lazy.layout.flip()),

# FLIP LAYOUT FOR BSP
    Key("M-A-k", lazy.layout.flip_up()),
    Key("M-A-j", lazy.layout.flip_down()),
    Key("M-A-l", lazy.layout.flip_right()),
    Key("M-A-h", lazy.layout.flip_left()),

# MOVE WINDOWS UP OR DOWN BSP LAYOUT
    Key("M-S-k", lazy.layout.shuffle_up()),
    Key("M-S-j", lazy.layout.shuffle_down()),
    Key("M-S-h", lazy.layout.shuffle_left()),
    Key("M-S-l", lazy.layout.shuffle_right()),

         ### Treetab controls
    Key("M-C-k",
            lazy.layout.section_up(),
            desc='Move up a section in treetab'
            ),
    Key("M-C-j",
            lazy.layout.section_down(),
            desc='Move down a section in treetab'
            ),

    # SCREEN MOVEMENTS
    Key("M-<period>", move_toscreen_right),
    Key("M-<comma>", move_toscreen_left),


# MOVE WINDOWS UP OR DOWN MONADTALL/MONADWIDE LAYOUT
    Key("M-S-<Up>", lazy.layout.shuffle_up()),
    Key("M-S-<Down>", lazy.layout.shuffle_down()),
    Key("M-S-<Left>", lazy.layout.swap_left()),
    Key("M-S-<Right>", lazy.layout.swap_right()),

# TOGGLE FLOATING LAYOUT
    Key("M-S-<space>", lazy.window.toggle_floating()),
]

# MOUSE CONFIGURATION
mouse = [
    Drag("M-1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag("M-3", lazy.window.set_size_floating(),
         start=lazy.window.get_size())
]
