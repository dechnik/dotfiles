# instantOS autostart script
# This script gets executed when lukasz logs in
# Add & (a literal) ampersand to the end of a line to make it run in the background

xset r rate 300 50 &
pamac-tray &
pasystray &
/home/lukasz/Apps/Nextcloud-3.1.3-x86_64.AppImage &
