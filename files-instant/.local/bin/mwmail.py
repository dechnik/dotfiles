#!/usr/bin/env python
import os

mail_dir = '~/.local/share/mail'
accounts_dir = '~/.config/mutt/accounts'
inbox = 'INBOX'
exclude = ['.notmuch']
accounts = {}

accounts_dir = os.path.expanduser(accounts_dir)
for f in os.listdir(accounts_dir):
    tmp = f.split('-')
    nr = tmp[0]
    name = tmp[1].replace('.muttrc','')
    accounts[name] = nr


mail_dir = os.path.expanduser(mail_dir)
data = ""
for dr in os.listdir(mail_dir):
    mail_count = 0
    if dr not in exclude:
        path = os.path.join(mail_dir,dr,inbox,'new')
        for file in os.listdir(path):
            mail_count += 1
        if mail_count > 0:
            data += "{}:{}  ".format(accounts[dr], mail_count)
print('^c#7c6f64^  M ' + data + '^d^')
