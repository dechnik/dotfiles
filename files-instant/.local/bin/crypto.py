#!/usr/bin/env python
import os
import requests

r = requests.get('https://rate.sx/1xmr')

price = float(r.json())

print('^c#928374^  XMR ' + f'{price:.2f}' + '  ^d^')
